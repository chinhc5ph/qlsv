using API.Data;
using Microsoft.AspNetCore.Mvc;


namespace API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class DeleteStudentController : ControllerBase
    {
        private readonly DataContext _context;
        public DeleteStudentController(DataContext context)
        {
            _context = context;
        }

        [HttpDelete("{studentcode}")]
        public void Romove(string studentcode)
        { if( studentcode!= null)
        {
             _context.Students.Remove(_context.Students.Find(studentcode));
             _context.SaveChanges();
        }
        }
    }
}
