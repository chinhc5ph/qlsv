using System.Collections.Generic;
using System.Linq;
using API.Data;
using API.Entities;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class StudentController : ControllerBase
    {
        private readonly DataContext _context;
        public StudentController(DataContext context)
        {
            this._context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AppStudent>>GetStudents()
        {
            return  _context.Students.ToList();
        }
    }
}