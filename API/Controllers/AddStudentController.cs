using System;
using API.Data;
using API.Entities;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class AddStudentController : ControllerBase
    {
        private readonly DataContext _context;
        public AddStudentController(DataContext context)
        {
            _context = context;
        }


        //Post
        [HttpPost("{Add}")] 
        public void Add(AppStudent StudentAdd)
        {   
            _context.Students.Add(StudentAdd);
            _context.SaveChangesAsync();
        }

        
    }
}