using API.Data;
using API.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Linq;


namespace API.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class EditStudentController : ControllerBase
    {
        private readonly DataContext _context;
        public EditStudentController(DataContext context)
        {
            this._context = context;
        }

        [HttpPost("{Edit}")]
        public void  Edit1(AppStudent StudentEdit)
        {
            AppStudent edit = _context.Students.Where(e => e.StudentCode == StudentEdit.StudentCode).FirstOrDefault();
            if (edit != null) {
                edit.FullName = StudentEdit.FullName;
                edit.StudentCode = StudentEdit.StudentCode;
                edit.StudentClass = StudentEdit.StudentClass;
            }
            _context.Students.Update(edit);
            _context.SaveChanges();
        }
    }
}