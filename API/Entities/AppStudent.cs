using System.ComponentModel.DataAnnotations;

namespace API.Entities
{
    public class AppStudent
    {  [Key]
        public string StudentCode { get; set; } 
        public string FullName { get; set; }
        public string StudentClass { get; set; }

      
    }
}