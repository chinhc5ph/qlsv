import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Student } from 'src/environments/student.model';

@Component({
  selector: 'app-editstudent',
  templateUrl: './editstudent.component.html',
  styleUrls: ['./editstudent.component.css']
})
export class EditstudentComponent implements OnInit {
  @Input() Students: any;
  @Input() StEdit: any;
  @Input() State: any;
  @Input() StateButton: any;
  @Output() onEditStudent = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  StudentB : Student = {

    FullName: '',
    StudentCode: '',
    StudentClass: ''
  }


  _FullName: string="";
  _StudentClass: string="";



    EditFullName(_FullName: any){
      this._FullName=_FullName;
    }

    EditStudentClass(_StudentClass: any){
      this._StudentClass=_StudentClass;
    }

    EditStudent(){
      this.StudentB.StudentCode=this.StEdit.studentCode;
      this.StudentB.FullName=this._FullName;
      this.StudentB.StudentClass=this._StudentClass;
      this.onEditStudent.emit(this.StudentB);
      this.State.Edit=0;
      this.StateButton.EditButton=0;
    }

}
