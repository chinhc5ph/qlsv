import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Student } from 'src/environments/student.model';

@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent implements OnInit {
  @Input() Students: any;
  @Output() onAddStudent =new EventEmitter();
  @Input() State: any;

  constructor() { }

  ngOnInit(): void {
  }

  StudentA : Student = {
   
    FullName: '',
    StudentCode: '',
    StudentClass: ''
  }

  _StudentCode: string="";
  _FullName: string="";
  _StudentClass: string="";

    AddStudentCode(_StudentCode: any){
        this._StudentCode=_StudentCode;
    }

    AddFullName(_FullName: any){
      this._FullName=_FullName;
    }

    AddStudentClass(_StudentClass: any){
      this._StudentClass=_StudentClass;
    }

    AddStudent(){
      this.StudentA.StudentCode=this._StudentCode;
      this.StudentA.FullName=this._FullName;
      this.StudentA.StudentClass=this._StudentClass;
      this.onAddStudent.emit(this.StudentA);
      this.State.Add=0;
    }
}
