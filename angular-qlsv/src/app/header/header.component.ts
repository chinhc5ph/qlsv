import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() State: any;
  @Input() StateButton: any;


  constructor() { }

  ngOnInit(): void {
  }

  StateAdd(){
    this.State.Add=1;
  }


  Editbutton(){
    this.StateButton.EditButton=1;
  }

  _delete: number=1;
  StateDelete(){
    this.StateButton.DeleteButton=1;
  }
}
