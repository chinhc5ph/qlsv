import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css'],
})
export class BodyComponent implements OnInit {
  @Input() Students: any;
  @Input() State: any;
  @Input() StateButton: any;
  @Output() onSentId = new EventEmitter();
  @Output() onDeleteId= new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  EditStudent(student: any) {

    this.onSentId.emit(student);
    this.State.Edit = 1;

  }


  DeleteStudent(student: any ) {

    alert(
      'Bạn có chắc chắn muốn xóa sinh viên:' + student.fullName+" - "+student.studentCode );

     this.StateButton.DeleteButton=0;
      this.onDeleteId.emit(student);
  }
}
