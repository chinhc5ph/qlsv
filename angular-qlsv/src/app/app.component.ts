import { Student } from './../environments/student.model';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  students: any;

  //Kiểm soát hiển thị từng phần
  state = {
    Add: 0,
    Edit: 0,
    Delete: 0,
    Table: 1,
  };

  statebutton={
    DeleteButton:0,
    EditButton:0
  }


  AddStudent(studentAdd: any) {
    var x=1;
     this.students.map(
      function(_student: any)
      {
          if(_student.studentCode==studentAdd.StudentCode)
            {
              x=0;
            }
      });

      if(x==0)
      {
        alert("Mã số sinh viên đã tồn tại..!");
      }
      else
      {
        this.http
      .post('https://localhost:5001/api/AddStudent/Add', studentAdd)
      .subscribe(
        () => {
          this.getStudents();
          this.state.Add = 0;
          this.state.Table = 1;
        },
        (error) => {
          console.log(error);

        }
      );
      }

  }

  StudentABC: any;

  SentId(studentEdit: any) {
    this.StudentABC = studentEdit;
    this.state.Edit = 1;
  }

  EditStudent(studentEdit: any) {
      console.log(studentEdit);
    this.http
      .post('https://localhost:5001/api/EditStudent/Edit', studentEdit)
      .subscribe(
        () => {
          this.getStudents();
        },
        (error) => {

          console.log(error);
        }
      );
  }

  editbutton: number = 0;
  Editbutton(_editbutton: any) {
    this.editbutton = 1;
  }

  delete: number = 0;
  StateDelete(_delete: any) {
    this.delete = _delete;
  }

  DeleteStudent(student: any) {

    this.http
      .delete("https://localhost:5001/api/DeleteStudent/"+ student.studentCode)
      .subscribe(
        () => {
          this.getStudents();
        },
        (error) => {
          console.log(error);
        }
      );
  }

  //Constructor khai bao http
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.getStudents();
  }

  //Day qua Database
  getStudents() {
    this.http.get('https://localhost:5001/api/Student').subscribe(
      (response) => {

        this.students = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
