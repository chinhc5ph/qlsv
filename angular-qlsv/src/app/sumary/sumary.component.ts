import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-sumary',
  templateUrl: './sumary.component.html',
  styleUrls: ['./sumary.component.css']
})
export class SumaryComponent implements OnInit {
  @Input() Students: any;

  constructor() { }

  ngOnInit(): void {
  }

}
